﻿using UnityEngine;
using System.Collections.Generic;


/**
 * The terrain manager.
 * 
 * This terrain manager uses sets of tiles to continuously generate terrain in front of the player as the player moves down the positive z axis.
 * 
 **/
public class TerrainManager
{
    /**
     * A data structure to contain the terrain tiles.
     * 
     **/
    [System.Serializable]
    public class LandscapeTiles
    {
        public GameObject[] start;
        public GameObject[] straight;
        public GameObject[] turnLeft;
        public GameObject[] turnRight;
        public GameObject[] turnBoth;
    }


    // The tile types.
    enum TileType
    {
        start,
        straight,
        turnLeft,
        turnRight,
        turnBoth,
        count
    };



    float slopeDegrees = 30.0f;



    #region ---PRIVATE VARIABLES---
    /*************************************************
     * PRIVATE VARIABLES
     *************************************************/

    // The player game object.
    GameObject player;


    // The landscape tiles used to build the landscape.
    LandscapeTiles landscapeTiles;

    // Queues used to hold unused tiles.
    Queue<GameObject> unusedStraightTiles;
    Queue<GameObject> unusedTurnLeftTiles;
    Queue<GameObject> unusedTurnRightTiles;
    Queue<GameObject> unusedTurnBothTiles;

    struct TileData
    {
        public GameObject tile;
        public TileType type;
        public bool turnLeft;
    }

    // The second level future tile information.
    TileData[] futureTiles2;

    // The future tile information.
    TileData[] futureTiles;

    // The current tile information.
    TileData currentTile;

    // The previous tile information.
    TileData previousTile;

    // The previously recorded player tile z index.
    int previousPlayerTileZIndex;

    // The start position.  Found from the startposition object in the start tile.
    Vector3 startPosition;

    // Tile length.
    float tileLength;

    // X offset for turns.
    float turnXOffset;

    // Flag indicating the branch decision needs to be made.
    bool checkBranchFuture = false;


    #endregion

    GameObject[] npcGameObjects;


    /**
     * Constructor.
     * 
     * Class variables are initialized and the initial landscape is created.
     * 
     **/
    public TerrainManager(GameObject player, GameObject[] npcGameObjects, LandscapeTiles landscapeTiles)
    {
        this.player = player;
        this.landscapeTiles = landscapeTiles;
        this.npcGameObjects = npcGameObjects;

        unusedTurnLeftTiles = new Queue<GameObject>();
        unusedTurnRightTiles = new Queue<GameObject>();
        unusedTurnBothTiles = new Queue<GameObject>();
        unusedStraightTiles = new Queue<GameObject>();

        futureTiles = new TileData[2];
        futureTiles2 = new TileData[2];

        CreateLandscape();
    }













    /**
     * Updates the landscape according to the player position.
     * 
     **/
    public void Update()
    {

        // Check if the player has changed tiles, and handle if necessary.
        int currentPlayerTileZIndex = ComputePlayerCurrentTileZIndex();
        if (currentPlayerTileZIndex != previousPlayerTileZIndex)
        {
            AdvanceTiles(currentPlayerTileZIndex);
            previousPlayerTileZIndex = currentPlayerTileZIndex;
        }


        // If the current tile is a branch and the branch decision hasn't been handled yet, handle, and potentially clear the flag.
        if (checkBranchFuture)
        {
            HandleCheckBranchFuture(currentPlayerTileZIndex);
        }

        CheckForZReset();


    }










    /**
     * Get the player start position.
     * 
     **/
    public Vector3 GetStartPosition()
    {
        return startPosition;
    }








    /**
     * Returns the ideal position and rotation for the player.
     * 
     **/
    public void GetIdealWSTransform(Vector3 inPos, ref Vector3 position, ref Quaternion rotation)
    {
        TileData wsTile;

        int n = (int)((inPos.z - currentTile.tile.transform.position.z) / tileLength);
        switch (n)
        {
            case -1:
                wsTile = previousTile;
                break;
            case 0:
                wsTile = currentTile;
                break;
            default:
                if (futureTiles[0].tile != null)
                {
                    wsTile = futureTiles[0];
                }
                else
                {
                    wsTile = futureTiles[1];
                }
                break;
        }


        // Get the player's progression across the tile in a ratio.
        float progression = (inPos.z - wsTile.tile.transform.position.z) / tileLength;
        Vector3 pos = wsTile.tile.transform.position;
        pos.z = inPos.z;

        pos.y += -tileLength * Mathf.Tan(Mathf.Deg2Rad * slopeDegrees) * progression + 30 ;

        // Compute the ideal position and rotation depending on the current tile type.
        switch (wsTile.type)
        {
            case TileType.straight:
                rotation = Quaternion.Euler(0, 0, 0);
                break;
            case TileType.turnLeft:
                pos.x -= (turnXOffset * progression);
                rotation = Quaternion.Euler(0, -45, 0);
                break;
            case TileType.turnRight:
                pos.x += (turnXOffset * progression);
                rotation = Quaternion.Euler(0, 45, 0);
                break;
            case TileType.turnBoth:
                if (wsTile.turnLeft)
                {
                    pos.x -= (turnXOffset * progression);
                    rotation = Quaternion.Euler(0, -45, 0);
                }
                else
                {
                    pos.x += (turnXOffset * progression);
                    rotation = Quaternion.Euler(0, 45, 0);
                }
                break;
        }
        position = pos;



        // If the player is almost all the way across this tile, then interpolate the rotation and position
        // using the next tile's values.
        float startInterpProgression = 0.5f;
        if (progression > startInterpProgression)
        {
            TileData nextTile;
            if (wsTile.type == TileType.turnBoth)
            {
                if (wsTile.turnLeft)
                {
                    if (n == 0)
                    {
                        nextTile = futureTiles[0];
                    }
                    else
                    {
                        nextTile = futureTiles2[0];
                    }
                }
                else
                {
                    if (n == 0)
                    {
                        nextTile = futureTiles[1];
                    }
                    else
                    {
                        nextTile = futureTiles2[1];
                    }
                }
            }
            else
            {
                if (futureTiles[0].tile)
                {
                    nextTile = futureTiles[0];
                }
                else
                {
                    nextTile = futureTiles[1];
                }
            }

            Quaternion futureRotation = Quaternion.Euler(0, 0, 0);
            Vector3 futurePosition = position;
            switch (nextTile.type)
            {
                case TileType.turnLeft:
                    futureRotation = Quaternion.Euler(0, -45, 0);
                    break;
                case TileType.turnRight:
                    futureRotation = Quaternion.Euler(0, 45, 0);
                    break;
                case TileType.turnBoth:
                    if (nextTile.turnLeft)
                    {
                        futureRotation = Quaternion.Euler(0, -45, 0);
                    }
                    else
                    {
                        futureRotation = Quaternion.Euler(0, 45, 0);
                    }
                    break;
            }
            rotation = Quaternion.Lerp(rotation, futureRotation, (progression - startInterpProgression) / (1.0f - startInterpProgression));
        }
    }










    /**
     * Advance the tile pointers.
     *
     **/
    private void AdvanceTiles(int currentPlayerTileZIndex)
    {
        // Dispose of the behind tile.
        if (previousTile.tile)
        {
            RemoveTile(previousTile.type, previousTile.tile);
        }


        // Set the previous tile to the current tile.
        previousTile.tile = currentTile.tile;
        previousTile.type = currentTile.type;


        // Set the proper future tile to the current tile.
        // If the current tile is a branch, one of the future tiles will have been removed at this point.
        if (futureTiles[0].tile)
        {
            currentTile.tile = futureTiles[0].tile;
            currentTile.type = futureTiles[0].type;
        }
        else
        {
            currentTile.tile = futureTiles[1].tile;
            currentTile.type = futureTiles[1].type;
        }


        // If the current tile is turn both, then check for the branch midway through.
        if (currentTile.type == TileType.turnBoth)
        {
            checkBranchFuture = true;
        }


        // Set the future tiles to the future2 tiles.
        futureTiles[0].type = futureTiles2[0].type;
        futureTiles[0].tile = futureTiles2[0].tile;
        if (futureTiles2[1].tile)
        {
            futureTiles[1].tile = futureTiles2[1].tile;
            futureTiles[1].type = futureTiles2[1].type;
        }
        else
        {
            futureTiles[1].tile = null;
        }


        // Compute the future2 tiles.
        ComputeFutureTile2(0, currentPlayerTileZIndex);


        // If the future tile is a branch, then two future2 types need to be created.
        TileType tileType;
        if (futureTiles[0].tile)
        {
            tileType = futureTiles[0].type;
        }
        else
        {
            tileType = futureTiles[1].type;
        }
        if ((tileType == TileType.turnBoth) || (currentTile.type == TileType.turnBoth))
        {
            ComputeFutureTile2(1, currentPlayerTileZIndex);
        }
        else
        {
            futureTiles2[1].tile = null;
        }
    }







    private void SlopeTile(GameObject tile)
    {
        MeshFilter meshFilter = tile.GetComponent<MeshFilter>();
        Mesh mesh = meshFilter.mesh;
        Vector3[] vertices = mesh.vertices;
        int i = 0;
        while (i < vertices.Length)
        {
            vertices[i].z += vertices[i].y * Mathf.Tan(Mathf.Deg2Rad * slopeDegrees);
            i++;
        }
        mesh.vertices = vertices;
        mesh.RecalculateBounds();
    }



    /**
     * Creates the initial landscape.
     * 
     **/
    private void CreateLandscape()
    {
        // Create the current tile.
        currentTile.tile = GameObject.Instantiate(landscapeTiles.start[0]);
        SlopeTile(currentTile.tile);
        currentTile.type = TileType.start;
        startPosition = currentTile.tile.transform.Find("StartPosition").position;
        tileLength = currentTile.tile.GetComponent<Renderer>().bounds.size.x;
        turnXOffset = 160.0f;


        // Create the future tiles.
        
        futureTiles[0].tile = GameObject.Instantiate(landscapeTiles.straight[(int)(Random.value * landscapeTiles.straight.Length)]);
        SlopeTile(futureTiles[0].tile);
        Vector3 pos = Vector3.zero;
        pos.z = tileLength;
        pos.y = -tileLength * Mathf.Tan(Mathf.Deg2Rad * slopeDegrees);
        futureTiles[0].tile.transform.position = pos;
        futureTiles[0].type = TileType.straight;

        futureTiles2[0].tile = GameObject.Instantiate(landscapeTiles.straight[(int)(Random.value * landscapeTiles.straight.Length)]);
        SlopeTile(futureTiles2[0].tile);
        pos = Vector3.zero;
        pos.z = tileLength * 2;
        pos.y = -tileLength * 2.0f * Mathf.Tan(Mathf.Deg2Rad * slopeDegrees);
        futureTiles2[0].tile.transform.position = pos;
        futureTiles2[0].type = TileType.straight;
    }








    /**
     * Handles removing and replacing tiles once the midpoint of a branch is reached.
     * 
     **/
    private void HandleCheckBranchFuture(int currentPlayerTileZIndex)
    {
        // If the player has crossed half way through the current tile,
        // adjust the future2 tiles to match the direction taken.
        if (player.transform.position.z > (currentPlayerTileZIndex + 1) * tileLength - (tileLength * 0.7f))
        {
            checkBranchFuture = false;
//            bool makingLeft = player.transform.position.x < currentTile.tile.transform.position.x;     // Note use this when not auto controlled.
            bool makingLeft = currentTile.turnLeft;


            // If the player is making a left, then remove any tiles to the right.
            if (makingLeft)
            {
                if (futureTiles[0].tile && futureTiles[0].type == TileType.turnBoth)
                {
                    Vector3 p = futureTiles[0].tile.transform.position;
                    p.x += turnXOffset;
                    p.z += tileLength;
                    p.y = -(currentPlayerTileZIndex + 2) * tileLength * Mathf.Tan(Mathf.Deg2Rad * slopeDegrees);
                    futureTiles2[1].tile.transform.position = p;
                }
                else
                {
                    RemoveTile(futureTiles2[1].type, futureTiles2[1].tile);
                    futureTiles2[1].tile = null;
                }
                RemoveTile(futureTiles[1].type, futureTiles[1].tile);
                futureTiles[1].tile = null;
            }
            else
            {
                if (futureTiles[1].tile && futureTiles[1].type == TileType.turnBoth)
                {
                    Vector3 p = futureTiles[1].tile.transform.position;
                    p.x -= turnXOffset;
                    p.z += tileLength;
                    p.y = -(currentPlayerTileZIndex + 2) * tileLength * Mathf.Tan(Mathf.Deg2Rad * slopeDegrees);
                    futureTiles2[0].tile.transform.position = p;
                }
                else
                {
                    RemoveTile(futureTiles2[0].type, futureTiles2[0].tile);
                    futureTiles2[0].tile = null;
                }

                RemoveTile(futureTiles[0].type, futureTiles[0].tile);
                futureTiles[0].tile = null;
            }
        }
    }










    /**
     * Computes the second level future tiles.
     * 
     **/
    private void ComputeFutureTile2(int idx, int currentPlayerTileZIndex)
    {
        // Compute the future tile type.
        futureTiles2[idx].type = (TileType)(int)(1 + Random.value * ((int)TileType.count - 1));

        // XXX test
/*
//        futureTiles2[idx].type = TileType.straight;
        if (futureTiles2[idx].type == TileType.turnBoth)
        {
            futureTiles2[idx].type = TileType.turnLeft;
        }
*/

        // Get the tile depending on the tile type.
        Vector3 p;
        switch (futureTiles2[idx].type)
        {
            // Straight.
            case TileType.straight:
                if (unusedStraightTiles.Count != 0)
                {
                    futureTiles2[idx].tile = unusedStraightTiles.Dequeue();
                }
                else
                {
                    futureTiles2[idx].tile = GameObject.Instantiate(landscapeTiles.straight[(int)(Random.value*landscapeTiles.straight.Length)]);
                    SlopeTile(futureTiles2[idx].tile);
                }
                break;
            case TileType.turnLeft:
                if (unusedTurnLeftTiles.Count != 0)
                {
                    futureTiles2[idx].tile = unusedTurnLeftTiles.Dequeue();
                }
                else
                {
                    futureTiles2[idx].tile = GameObject.Instantiate(landscapeTiles.turnLeft[(int)(Random.value * landscapeTiles.turnLeft.Length)]);
                    SlopeTile(futureTiles2[idx].tile);
                }
                break;
            case TileType.turnRight:
                if (unusedTurnRightTiles.Count != 0)
                {
                    futureTiles2[idx].tile = unusedTurnRightTiles.Dequeue();
                }
                else
                {
                    futureTiles2[idx].tile = GameObject.Instantiate(landscapeTiles.turnRight[(int)(Random.value * landscapeTiles.turnRight.Length)]);
                    SlopeTile(futureTiles2[idx].tile);
                }
                break;
            case TileType.turnBoth:
                if (unusedTurnBothTiles.Count != 0)
                {
                    futureTiles2[idx].tile = unusedTurnBothTiles.Dequeue();
                }
                else
                {
                    futureTiles2[idx].tile = GameObject.Instantiate(landscapeTiles.turnBoth[(int)(Random.value * landscapeTiles.turnBoth.Length - 1)]);
                    SlopeTile(futureTiles2[idx].tile);
                }
                if (Random.value > 0.5f)
                {
                    futureTiles2[idx].turnLeft = true;
                }
                else
                {
                    futureTiles2[idx].turnLeft = false;
                }
                break;
        }


        // Set the position.  Take into account "turn both" tiles before.
        float xOffset = 0;
        TileType tileType;
        if (idx == 0)
        {
            if (futureTiles[0].tile)
            {
                tileType = futureTiles[0].type;
            }
            else
            {
                tileType = futureTiles[1].type;
            }
        }
        else
        {
            if (futureTiles[1].tile)
            {
                tileType = futureTiles[1].type;
            }
            else
            {
                tileType = futureTiles[0].type;
            }
        }
        switch (tileType)
        {
            case TileType.turnBoth:
                if (idx == 0)
                {
                    xOffset -= turnXOffset;
                }
                else
                {
                    xOffset += turnXOffset;
                }
                break;
            case TileType.turnLeft:
                xOffset -= turnXOffset;
                break;
            case TileType.turnRight:
                xOffset += turnXOffset;
                break;
        }
        switch (currentTile.type)
        {
            case TileType.turnBoth:
                if (idx == 0)
                {
                    xOffset -= turnXOffset;
                }
                else
                {
                    xOffset += turnXOffset;
                }
                break;
            case TileType.turnLeft:
                xOffset -= turnXOffset;
                break;
            case TileType.turnRight:
                xOffset += turnXOffset;
                break;
        }


        p = Vector3.zero;
        p.x = currentTile.tile.transform.position.x + xOffset;
        p.z = (currentPlayerTileZIndex + 2) * tileLength;
        p.y = -(currentPlayerTileZIndex+2) * tileLength * Mathf.Tan(Mathf.Deg2Rad * slopeDegrees);
        futureTiles2[idx].tile.transform.position = p;
        futureTiles2[idx].tile.SetActive(true);
    }






    /**
     * Removes the specified tile.
     * 
     **/
    private void RemoveTile(TileType tileType, GameObject tile)
    {
        switch (tileType)
        {
            case TileType.straight:
                unusedStraightTiles.Enqueue(tile);
                break;
            case TileType.turnLeft:
                unusedTurnLeftTiles.Enqueue(tile);
                break;
            case TileType.turnRight:
                unusedTurnRightTiles.Enqueue(tile);
                break;
            case TileType.turnBoth:
                unusedTurnBothTiles.Enqueue(tile);
                break;
        }
        tile.SetActive(false);
    }








    /**
     * Compute the current player tile z index.
     * 
     **/
    private int ComputePlayerCurrentTileZIndex()
    {
        return (int)((player.transform.position.z) / tileLength);
    }


    private void CheckForZReset()
    {

        float maxZ = tileLength * 5;
        if (player.transform.position.z > maxZ)
        {
            float yOffset = previousTile.tile.transform.position.y;
            float zOffset = previousTile.tile.transform.position.z;
            float xOffset = previousTile.tile.transform.position.x;
            previousPlayerTileZIndex -= (int)(zOffset / tileLength);
            Vector3 pos = player.transform.position;
            pos.z -= zOffset;
            pos.y -= yOffset;
            pos.x -= xOffset;
            player.transform.position = pos;

            for (int i=0; i<npcGameObjects.Length; ++i)
            {
                Vector3 p = npcGameObjects[i].transform.position;
                p.z -= zOffset;
                p.y -= yOffset;
                p.x -= xOffset;
                npcGameObjects[i].transform.position = p;
            }




            pos = previousTile.tile.transform.position;
            pos.z -= zOffset;
            pos.y -= yOffset;
            pos.x -= xOffset;
            previousTile.tile.transform.position = pos;

            pos = currentTile.tile.transform.position;
            pos.z -= zOffset;
            pos.y -= yOffset;
            pos.x -= xOffset;
            currentTile.tile.transform.position = pos;

            if (futureTiles[0].tile)
            {
                pos = futureTiles[0].tile.transform.position;
                pos.z -= zOffset;
                pos.y -= yOffset;
                pos.x -= xOffset;
                futureTiles[0].tile.transform.position = pos;
            }
            if (futureTiles[1].tile)
            {
                pos = futureTiles[1].tile.transform.position;
                pos.z -= zOffset;
                pos.y -= yOffset;
                pos.x -= xOffset;
                futureTiles[1].tile.transform.position = pos;
            }
            if (futureTiles2[0].tile)
            {
                pos = futureTiles2[0].tile.transform.position;
                pos.y -= yOffset;
                pos.z -= zOffset;
                pos.x -= xOffset;
                futureTiles2[0].tile.transform.position = pos;
            }
            if (futureTiles2[1].tile)
            {
                pos = futureTiles2[1].tile.transform.position;
                pos.z -= zOffset;
                pos.y -= yOffset;
                pos.x -= xOffset;
                futureTiles2[1].tile.transform.position = pos;
            }
        }
    }


}





