﻿using UnityEngine;
using System.Collections;



/**
 * This is the main class for the Skydive app.
 *
 **/
public class Skydive : MonoBehaviour
{
    #region ---PUBLIC INTERFACE VARIABLES---
    /*************************************************
     * PUBLIC INTERFACE VARIABLES
     *************************************************/

    [Tooltip("The player GameObject.")]
    public GameObject player;

    public Camera gameCamera;

    [Tooltip("The landscape tiles.")]
    public TerrainManager.LandscapeTiles landscapeTiles;

    #endregion



    #region ---PRIVATE VARIABLES---
    /*************************************************
     * PRIVATE VARIABLES
     *************************************************/

    // The terrain manager.
    TerrainManager terrainManager;

    #endregion


    public GameObject[] npcGameObjects;


    /**
     * Inits the game.
     * 
     * Class variables are initialized.
     * 
     * Note: Use this for initialization.
     * 
     **/
    void Start ()
    {
        terrainManager = new TerrainManager(player, npcGameObjects, landscapeTiles);
        player.transform.position = terrainManager.GetStartPosition();
	}




    /**
     * Updates the start state.
     * 
     * The terrain manager is updated.
     * 
     * Note: Update is called once per frame by Unity.
     * 
     **/
    void Update ()
    {
        terrainManager.Update();
	}




    public TerrainManager GetTerrainManager()
    {
        return terrainManager;
    }


}
