﻿using UnityEngine;
using System.Collections;


/**
 * The Start game state.
 * 
 **/
public class StartGameState : MonoBehaviour
{

    #region ---PUBLIC INTERFACE VARIABLES---
    /*************************************************
     * PUBLIC INTERFACE VARIABLES
     *************************************************/

    #endregion



    #region ---PRIVATE VARIABLES---
    /*************************************************
     * PRIVATE VARIABLES
     *************************************************/

    // The Skydive object.
    Skydive skydive;


    #endregion




    class WingsuitObject
    {
        public GameObject gameObject;
        public Rigidbody rb;
        public Quaternion lookAtRotation = Quaternion.identity;
        public bool hasStarted = false;
        public float zRotation;

        public Vector3 autopilotOffset = new Vector3();
    }


    WingsuitObject player;

    WingsuitObject[] npc;

    float startTime;

    /**
     * Inits the start state.
     * 
     * Class variables are initialized.
     * 
     * Note: Use this for initialization.
     * 
     **/
    void Start ()
    {
        skydive = GetComponent<Skydive>();
        player = new WingsuitObject();
        player.gameObject = skydive.player;
        player.rb = skydive.player.GetComponent<Rigidbody>();
        npc = new WingsuitObject[2];
        npc[0] = new WingsuitObject();
        npc[1] = new WingsuitObject();
        npc[0].gameObject = skydive.npcGameObjects[0];
        npc[0].rb = npc[0].gameObject.GetComponent<Rigidbody>();
        npc[1].gameObject = skydive.npcGameObjects[1];
        npc[1].rb = npc[1].gameObject.GetComponent<Rigidbody>();
        npc[1].autopilotOffset.x = -5;
        npc[1].autopilotOffset.y = 10;
        startTime = Time.fixedTime;
    }


    //    Vector3 startvelocity = new Vector3(0, -49.1f, 84.9f);
    Vector3 startvelocity = new Vector3(0, 0, 0);



    void UpdateAutopilot(WingsuitObject ws)
    {
        // Get the ideal position and rotation.
        Vector3 idealPos = Vector3.zero;
        Quaternion idealRotation = Quaternion.identity;
        Vector3 p = ws.gameObject.transform.position - ws.autopilotOffset;
        skydive.GetTerrainManager().GetIdealWSTransform(p, ref idealPos, ref idealRotation);

        idealPos += ws.autopilotOffset;


        // Create a look vector rotated by the ideal rotation.
        Vector3 lookVector = new Vector3(0, 0, 1);
        lookVector = idealRotation * lookVector;


        // Add rotation about the X & Y axis to account for Y & X offset from ideal position.
        float positionInfluence = 1.2f;
        float xRot = -positionInfluence * (idealPos.y - ws.gameObject.transform.position.y);
        if (xRot > 80)
        {
            xRot = 80;
        }
        else if (xRot < -80)
        {
            xRot = -80;
        }
        float yRot = positionInfluence * (idealPos.x - ws.gameObject.transform.position.x);
        if (yRot > 80)
        {
            yRot = 80;
        }
        else if (yRot < -80)
        {
            yRot = -80;
        }


        // Set the look vector.
        lookVector = Quaternion.Euler(xRot, yRot, 0) * lookVector;


        // Lerp the autopilot rotation towards the look vector.
        Quaternion lookAtRotation = Quaternion.LookRotation(lookVector);
        ws.zRotation = yRot * 2.0f;
        if (ws.zRotation  > 90)
        {
            ws.zRotation = 90;
        } else if (ws.zRotation < -90)
        {
            ws.zRotation = -90;
        }
        float lookSpeed = 8.0f;
        ws.lookAtRotation = Quaternion.Lerp(ws.lookAtRotation, lookAtRotation, Time.deltaTime * lookSpeed);


    }

    
    void FixedUpdate()
    {
        if ((!npc[0].hasStarted) && (Time.fixedTime - startTime > 2.0f))
        {
            npc[0].hasStarted = true;
            npc[0].rb.useGravity = true;
        }
        if ((!npc[1].hasStarted) && (Time.fixedTime - startTime > 3.0f))
        {
            npc[1].hasStarted = true;
            npc[1].rb.useGravity = true;
        }
        if ((!player.hasStarted) && (Time.fixedTime - startTime > 4.0f))
        {
            player.hasStarted = true;
            player.rb.useGravity = true;
        }

        if (npc[0].hasStarted)
        {
/*
            if (Time.fixedTime % 1 == 0)
            {
                npc[0].autopilotOffset.x += (Random.value * 1.0f - 0.50f);
                npc[0].autopilotOffset.y += (Random.value * 1.40f - 0.50f);
            }
*/
            UpdateAutopilot(npc[0]);
            UpdateWSVelocityVector(npc[0]);
            npc[0].gameObject.transform.rotation = npc[0].lookAtRotation;
            npc[0].gameObject.transform.rotation = npc[0].gameObject.transform.rotation * Quaternion.Euler(0, 0, npc[0].zRotation);
        }

        if (npc[1].hasStarted)
        {
/*
            if (Time.fixedTime % 1 == 0)
            {
                npc[1].autopilotOffset.x += (Random.value * 1.0f - 0.50f);
                npc[1].autopilotOffset.y += (Random.value * 1.0f - 0.50f);
            }
*/
            UpdateAutopilot(npc[1]);
            UpdateWSVelocityVector(npc[1]);
            npc[1].gameObject.transform.rotation = npc[1].lookAtRotation;
            npc[1].gameObject.transform.rotation = npc[1].gameObject.transform.rotation * Quaternion.Euler(0, 0, npc[1].zRotation);
        }


        UpdateAutopilot(player);


        // Set the game camera's rotation to the autopilot look at rotation.
        skydive.gameCamera.transform.rotation = player.lookAtRotation;
        skydive.gameCamera.transform.rotation = skydive.gameCamera.transform.rotation * Quaternion.Euler(0, 0, player.zRotation);


        UpdateWSVelocityVector(player);






#if false

        /////////////////////////////////////////////



        // Rotate the player towards the ideal rotation.
        float positionInfluence = 0.1f;
        float lookSpeed = 8.0f;
        Quaternion q = Quaternion.Euler(0, positionInfluence * (idealPos.x - player.gameObject.transform.position.x), 0) * Quaternion.Lerp(player.gameObject.transform.rotation, idealRotation, Time.deltaTime * lookSpeed);


/*
        // XXX y offset rotation work.
        // XXX CAUSES Z ROTATION  ON NON STRAIGHT TILES!!!
        float yd = player.gameObject.transform.position.y - idealPos.y;
        q = Quaternion.Euler(yd * 0.2f, 0, 0) * q;
*/

        player.gameObject.transform.rotation = q;
        //        player.gameObject.transform.rotation = idealRotation;





        // Compute the angle between the ideal y rotation and the velocity y rotation.
        Vector3 v0 = new Vector3(0, 0, 1);
        v0 = player.gameObject.transform.rotation * v0;
        Vector3 v1 = new Vector3(result.x, 0, result.z);
        float dot = v0.x * v1.x + v0.y * v1.y + v0.z * v1.z;
        float magV0 = Mathf.Sqrt(v0.x * v0.x + v0.y * v0.y + v0.z * v0.z);
        float magV1 = Mathf.Sqrt(v1.x * v1.x + v1.y * v1.y + v1.z * v1.z);
        float dAngle = Mathf.Acos(dot / (magV0 * magV1));
        float x0 = v0.x / magV0;
        float x1 = v1.x / magV1;
        if (x0 < x1)
        {
            dAngle *= -1.0f;
        }
        // Rotate the velocity vector by the difference.
        result = Quaternion.Euler(0, dAngle, 0) * result;


/*
        // XXX y offset rotation work.
                v1 = new Vector3(0, result.y, result.z);
                dot = v0.x * v1.x + v0.y * v1.y + v0.z * v1.z;
                magV0 = Mathf.Sqrt(v0.x * v0.x + v0.y * v0.y + v0.z * v0.z);
                magV1 = Mathf.Sqrt(v1.x * v1.x + v1.y * v1.y + v1.z * v1.z);
                dAngle = Mathf.Acos(dot / (magV0 * magV1));
                float y0 = v0.y / magV0;
                float y1 = v1.y / magV1;
                if (y0 > y1)
                {
                    dAngle *= -1.0f;
                }
        // Rotate the velocity vector by the difference.
        result = Quaternion.Euler(dAngle, 0, 0) * result;
*/



        // Update the player's velocity.
        playerRB.velocity = result;
#endif
    }

    void UpdateWSVelocityVector(WingsuitObject ws)
    {
        Quaternion camRotation = ws.lookAtRotation;
        Vector3 velocity = ws.rb.velocity;
        Vector3 result = ws.rb.velocity;
        result = Quaternion.Euler(0.00000000000000000000001f * velocity.y, 0, 0) * velocity;
        Vector3 lookVector = camRotation * new Vector3(0, 0, 1);


        // Rotate the velocity vector toward the look vector.
        Vector3 v0 = lookVector;
        Vector3 v1 = new Vector3(result.x, 0, result.z);
        float dot = v0.x * v1.x + v0.y * v1.y + v0.z * v1.z;
        float magV0 = Mathf.Sqrt(v0.x * v0.x + v0.y * v0.y + v0.z * v0.z);
        float magV1 = Mathf.Sqrt(v1.x * v1.x + v1.y * v1.y + v1.z * v1.z);
        float dAngle = Mathf.Acos(dot / (magV0 * magV1));
        float x0 = v0.x / magV0;
        float x1 = v1.x / magV1;
        if (x0 < x1)
        {
            dAngle *= -1.0f;
        }
        // Rotate the velocity vector by the difference.
        result = Quaternion.Euler(0, dAngle, 0) * result;


        // XXX y offset rotation work.
        v1 = new Vector3(0, result.y, result.z);
        dot = v0.x * v1.x + v0.y * v1.y + v0.z * v1.z;
        magV0 = Mathf.Sqrt(v0.x * v0.x + v0.y * v0.y + v0.z * v0.z);
        magV1 = Mathf.Sqrt(v1.x * v1.x + v1.y * v1.y + v1.z * v1.z);
        dAngle = Mathf.Acos(dot / (magV0 * magV1));
        float y0 = v0.y / magV0;
        float y1 = v1.y / magV1;
        if (y0 > y1)
        {
            dAngle *= -1.0f;
        }
        // Rotate the velocity vector by the difference.
        result = Quaternion.Euler(dAngle, 0, 0) * result;


        ws.rb.velocity = result;
    }

    /**
     * Updates the start state.
     * 
     * The player position is updated.
     * 
     * Note: Update is called once per frame by Unity.
     * 
     **/
    void Update ()
    {

/*
        // Update the player position.
        Vector3 pos = player.gameObject.transform.position;
        pos.z += Time.deltaTime * 80.0f;
        player.gameObject.transform.position = pos;
        Vector3 idealPos = Vector3.zero;
        Quaternion idealRotation = Quaternion.identity;
        skydive.GetTerrainManager().GetIdealPlayerTransform(ref idealPos, ref idealRotation);

        player.gameObject.transform.position = idealPos;// Vector3.Lerp(player.gameObject.transform.position, idealPos, Time.deltaTime * 2.0f);
        player.gameObject.transform.rotation = Quaternion.Lerp(player.gameObject.transform.rotation, idealRotation, Time.deltaTime * 2.0f);
*/

    }
}


