# RailGame00 #

This project is to serve as the base project for all foreseeable rail games for O8C.

## Skydive (working title) ##

This is the first rail game to be developed with this project.  It is a wingsuit skydive game heavily influenced by Subway Surfers.

### Versions & Milestones ###

#### Version 0.1 ####
This version is to be a bare bones playable game.  A basic front end exists, and the user can start the game.  The user can control the player and crash into walls.  The game displays a game over screen and the user can restart the game.

##### Milestone 1: Start State #####
The user interface is of extreme importance in this game.  For this reason, and for the reason that the start screen encompasses many aspects of the game, the start state is the first milestone.

At the end of this milestone, the game will start with a splash screen.  This will fade out to black, that fades into a view.  The view that fades in is a view that moves through the landscape.  The landscape is generating as the viewpoint moves forward, just like in the game.  The viewpoint uses the optimal path to smoothly flow through the landscape, and randomly makes decisions at intersections.

##### Milestone 2: Play State Basics #####
It is important to get the game in a playable state as soon as possible to better understand the objective.  For this reason, the play state is the second milestone.
	
At the end of this milestone, the start state will display HUD elements, and display a countdown timer.  At the end of the countdown, a message is displayed, and the perspective is controlled by user input.  When the player collides with the environment, the perspective bounces back a bit and holds.  At this point, a screen is displayed with a retry button.  Upon clicking the retry button, the view fades out, then fades back in restarting the game.

##### Milestone XXX1: Polish #####
A final milestone will be to polish.